import React from 'react';
import { BrowserRouter, Routes , Route } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import LocationForm from './LocationForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  console.log(props.attendees)
  if(props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
          <Route index element={<MainPage />} />
          <Route path="presentations/new" element={<PresentationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
      </Routes>
    </div>
    </BrowserRouter>
  );
  
}
export default App;
      
       
        
          
          
      
        
      
       
      
        
      
  