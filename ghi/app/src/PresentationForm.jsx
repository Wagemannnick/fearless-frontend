import React, { Component } from 'react';

class PresentationForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title:'',
            synopsis:'',
            conferences: []};
        this.handlePresenterNameChange = this.handlePresenterNameChange.bind(this)
        this.handlePresenterEmailChange = this.handlePresenterEmailChange.bind(this)
        this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this)
        this.handleTitleChange = this.handleTitleChange.bind(this)
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this)
        this.handleConferenceChange = this.handleConferenceChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        data.presenter_email = data.presenterEmail;
        data.presenter_name = data.presenterName;
        data.company_name = data.companyName;
        delete data.companyName
        delete data.presenterEmail
        delete data.presenterName
        delete data.conferences;
        console.log(data);
        let conference = data.conference;

        // const selectTag = document.getElementById('conference');
        // const conferenceId = selectTag.options[selectTag.selectedIndex].value;
        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
          const newPresentation = await response.json();
          console.log(newPresentation);

        const cleared = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conference: '',
            
        };
        this.setState(cleared);
        }

        
    }

    handlePresenterNameChange(event){
        const value = event.target.value;
        this.setState({presenterName: value});
    }

    handlePresenterEmailChange(event){
        const value = event.target.value;
        this.setState({presenterEmail: value});
    }

    handleCompanyNameChange(event){
        const value = event.target.value;
        this.setState({companyName: value});
    }

    handleTitleChange(event){
        const value = event.target.value;
        this.setState({title: value});
    }

    handleSynopsisChange(event){
        const value = event.target.value;
        this.setState({synopsis: value});
    }
    
    handleConferenceChange(event){
        const value = event.target.value;
        this.setState({conference: value});
    }

    async componentDidMount(){
        
        const url = "http://localhost:8000/api/conferences/";

        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            this.setState({conferences: data.conferences});
        }
    }

    render() { 
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input value={this.state.presenterName} onChange={this.handlePresenterNameChange} placeholder="presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                    <label htmlFor="presenter name">Presenter Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.presenterEmail} onChange={this.handlePresenterEmailChange} placeholder="presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                    <label htmlFor="presenter email">Presenter Email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.companyName} onChange={this.handleCompanyNameChange} placeholder="company name" required type="text" name="company_name" id="company_name" className="form-control"/>
                    <label htmlFor="company">Company Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value={this.state.title} onChange={this.handleTitleChange} placeholder="title" required type="text" name="title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea value={this.state.synopsis} onChange={this.handleSynopsisChange} className="form-control" name="synopsis" id="synopsis" cols="30" rows="10"></textarea>
                    <label htmlFor="synopsis">Synopsis</label>
                  </div>
                  <div className="mb-3">
                    <select value={this.state.conference} onChange={this.handleConferenceChange} required id="conference" name="conference" className="form-select">
                      <option value="">Choose a Conference</option>
                        {this.state.conferences.map(conference => {
                            return (
                                <option key={conference.href} value={conference.href}>
                                    {conference.name}
                                </option>
                            )
                        })}
                    </select>
                    </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}
 
export default PresentationForm;